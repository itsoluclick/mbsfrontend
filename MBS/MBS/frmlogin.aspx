﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmlogin.aspx.cs" Inherits="MBS.frmlogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Css/login.css" rel="stylesheet" />
</head>
<body>
    <div class="clform ">
       
    <form id="form1" class="form" runat="server">
        <div>
          <img src="logo.jpg" class="image"  />
           <h2  style="color:rgb(155, 149, 149); border:none; margin-bottom:50px; text-align:center; margin-right:20px" >Login MBS</h2>
        <div class="cldiv">         
           <asp:TextBox ID="txtusuario" TabIndex="1" placeholder="Usuario"  AutoPostBack ="true"  CssClass="cltext"  AutoComplete="off"   runat ="server"></asp:TextBox>
            <asp:Label  CssClass="cllabel"   Text="Usuario" runat="server" />
        </div>
         <div class="cldiv">
           <asp:TextBox ID="txtpassword" TabIndex="2" placeholder="Password"  AutoPostBack ="true"  CssClass="cltext"  AutoComplete="off"  OnTextChanged="txtpassword_TextChanged"  TextMode="Password"  runat ="server"></asp:TextBox>
            <asp:Label  CssClass="cllabel"   Text="Password" runat="server" />
        </div>
        <asp:Button  ID="btsumit" Text="Sign In"  OnClick="btsumit_Click" CssClass=" sumit" runat="server" />
            </div>
    </form>
        </div>
      
</body>
</html>
