﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBS
{
    public partial class frmlogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                limpia();
            }
        }

      
        void limpia()
        {
            
                txtusuario.Text = string.Empty;
            txtpassword.Attributes.Remove("value");
        }

        protected void btsumit_Click(object sender, EventArgs e)
        {
            limpia();
        }

        protected void txtpassword_TextChanged(object sender, EventArgs e)
        {
            string password = "";
            if (txtpassword.Text.Length > 0)
            {
                password = txtpassword.Text;
                txtpassword.Attributes.Add("value",password);
            }
        }
    }
}